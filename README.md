# Baltvib_RF_RFE

Code for the machine learning performed in Baltvib - Spatial dataset 


<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
Written and concieved by David Riedinger (ORCID:0009-0002-0221-6772) for the Baltvib (Biodiversa) project lead by M. Labrenz.
With considerable contributions by Theo Sperlea (ORDID:0000-0003-4307-2963) 
and Christiane Hassenrueck (ORCID:0000-0003-1909-1726)
Under employment at Leibniz Institute für Ostseeforschung Warnemünde



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="BaltVib_Final_Logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">project_title</h3>

  <p align="center">
    project_description
    <br />
    <a href="https://github.com/github_username/repo_name"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/github_username/repo_name">View Demo</a>
    ·
    <a href="https://github.com/github_username/repo_name/issues">Report Bug</a>
    ·
    <a href="https://github.com/github_username/repo_name/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

This repository contains all the code used for machine learning in the forthcoming paper 'Riedinger et al., 2024 (in prep).' Additionally, you can access the bioinformatics pipeline at 'https://github.com/lfdelzam/ASV_dada2_chunck/' and find comprehensive metadata in the 'doi.io-warnemuende.de/10.12754/data-2023-0010' and the sequencing data at "link here" Stay tuned for the latest updates and contributions to our research project.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With

* R V.4.3.0 [https://www.r-project.org/]
* Caret V.6.0.94 [https://topepo.github.io/caret/]
* dplyr V.1.1.2 [https://dplyr.tidyverse.org/]
* Vegan V.2.6.4 [https://github.com/vegandevs/vegan]
* data.table V.1.14.8[https://rdatatable.gitlab.io/data.table/]
* compositions V.2.0.6 [http://www.stat.boogaart.de/compositions/]


<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

The project consists of 1 masterscript that perform the RF/RFE on the 1. ddPCR data  and 2. 16S derived cell counts (Target_16S_RF_RFE_Submission_draft.R) that need the following input:
* complete_metadata.csv
* ASV_counts.tsv
* silva_ASV_taxonomy.tsv
* SILVA_all_assig_species.tsv

And source the following scripts:
* OTU_vibTarget.R (which calculates the proportion of V. vulnificus in the sample)
* Eukaryote_feature_space.R (which prepares the 18S rRNA sequencing data for the algorithm)

These scripts don't need additional input. Their output it a performance metric for the RF and a list of most important features according to the RFE algorithm for both targets.
### The input files are currently still under embargo and not freely published - please contact me for access

### Prerequisites

There are no prerequisites besides R and the listed packages

<!-- USAGE EXAMPLES -->
## Usage

This documentation exists to allow other researcher to replicate/verify the work done an provide a base for other researchers who want to try a similar approach to their data. The metadata and sequencing files can be replaced with other files and the approach can be applied to other datasets with minimal reworking of the code.

_For more the associated paper, please refer to the (link here)_

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch 
3. Commit your Changes 
4. Push to the Branch 
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the BSD-3-Clause License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

David Riedinger / Matthias Labrenz - d.j.riedinger@gmail.com/david.riedinger@io-warnemuende.de 

Project Link: [Baltvib](https://www.io-warnemuende.de/baltvib-home-en.html)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

